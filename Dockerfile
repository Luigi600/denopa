# container
FROM python:2.7.18

RUN pip install numpy==1.16.5
RUN pip install scipy==1.2.3
RUN pip install h5py==2.10.0
RUN pip install pysam==0.18.0
RUN pip install sklearn
RUN pip install statsmodels==0.10.2
RUN git clone https://gitee.com/bxxu/denopa.git denopa
RUN cd denopa && \
    python setup.py install && \
	cp ./bin/denopa /usr/bin/denopa.py

ADD denopa.sh usr/bin/denopa

RUN ["chmod", "+x", "/usr/bin/denopa"]

CMD ["denopa"]
